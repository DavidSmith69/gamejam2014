﻿using UnityEngine;
using System.Collections;

public class HowToLevelScript : MonoBehaviour {

	public GUIStyle playTexture;
	public GUIStyle quitTexture;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}
	void OnGUI() {

		GUI.TextArea (new Rect ((Screen.width/2) - 400,300,800,300), "Peter! ... Peter are you there? ... Peter, you need to listen to me. You're in terrible danger of losing your mind. If we don't stop this soon we won't be able to stop you from closing the door on your sanity and walking out into the maddness... forever lost to all those who still love you, despite what you did.\n\nFirst we need to teach you how to move:\n\nUse the Left and Right Arrow Keys to move Horizontally across your dreamscape. \nThe Up arrow key will allow you to jump high.\n(Remember you can jump much higher if you remain an adult in your comatosed dream state)\nBy pressing the Spacebar, you will regress back to your child state based on memories from the past. You can use this to your advantage to access hard to reach areas.\nRemember Peter, do not stay a child for too long. These memories are the ones that are plaguing your mind and keeping you from us.\nStay too long as a child and we may lose you.\nIf you can conquer the demons that haunt you we should be able to facilitate your recovery.\nYou can survive this. Just remember that what is left of your sanity is your greatest ally.\nBut Peter, please.\n\n                                                                      DO NOT... TRUST THE CLOWN.");

	if (GUI.Button (new Rect ((Screen.width/2) -350 ,(Screen.height/2) + 200, 150, 50),"", playTexture)) {
		Application.LoadLevel ("FirstLevel");
		Debug.Log ("Clicked the Start Button");

	if (GUI.Button (new Rect ((Screen.width/2) -340 ,(Screen.height/2) + 200, 150, 50),"", quitTexture)) {
		Application.Quit();
		Debug.Log ("Clicked the Quit Button");
			}


				
		}
}
}