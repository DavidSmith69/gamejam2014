﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterScript : MonoBehaviour, ISubject {

	public GameObject gameCharacter;
	public bool isChild = false;
	private CharacterController characterControl;
	public float jumpSpeed = 20, gravity = 10, moveSpeed = 10;
	public Vector3 moveVector;
	public Material kidMaterial, adultMaterial;
	private List<IObserver> observers;
	public bool grounded;

	// Use this for initialization
	void Start () {
		characterControl = GetComponent("CharacterController") as CharacterController;
		moveVector = new Vector3();
		observers = new List<IObserver>();
		switchToAdult();
	}
	
	// Update is called once per frame
	void Update () 
	{
		characterControl.Move(moveVector*Time.deltaTime);
		
		moveVector.y -= gravity*Time.deltaTime;

		if(transform.position.y <= gameObject.transform.localScale.y/2)
		{
			transform.position = new Vector3(transform.position.x, gameObject.transform.localScale.y/2, transform.position.z);
			grounded = true;
		}

		if(characterControl.velocity.y == 0)
			grounded = true;
	}

	public void switchToChild()
	{
		isChild = true;
		renderer.material = kidMaterial;
		notifyObservers();
	}

	public void switchToAdult()
	{
		isChild = false;
		renderer.material = adultMaterial;
		notifyObservers();
	}

	public void addObserver(IObserver observer)
	{
		observers.Add(observer);
	}

	public void removeObserver(IObserver observer)
	{
		observers.Remove(observer);
	}

	private void notifyObservers()
	{
		foreach(IObserver observer in observers)
		{
			observer.notify();
		}
	}
}