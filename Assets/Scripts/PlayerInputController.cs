using UnityEngine;
using System.Collections;

public class PlayerInputController : MonoBehaviour
{
	private IPlayerInput inputType;
	private CharacterScript character;

	void Start ()
	{
		inputType = new KeyboardGamePlayerInput();
		character = GetComponent("CharacterScript") as CharacterScript;
	}

	void Update ()
	{
		if(inputType.right())
		{
			character.moveVector.x = character.moveSpeed;
		}
		if(Input.GetKeyUp(KeyCode.RightArrow))
		{
			character.moveVector.x = 0;
		}
		
		if(inputType.left())
		{
			character.moveVector.x = -character.moveSpeed;
		}
		if(Input.GetKeyUp(KeyCode.LeftArrow))
		{
			character.moveVector.x = 0;
		}

		if(inputType.up())
		{
			if(character.grounded)
			{
				character.moveVector.y = character.jumpSpeed;
				character.grounded = false;
			}
		}
		
		if(inputType.down())
		{
			//Crouch
		}
		
		if(inputType.pause())
		{
			//Pause menu
		}
		
		if(inputType.resume())
		{
			//Resume Game
		}
		
		if(inputType.switchPersona())
		{
			if (character.isChild) 
			{
				character.switchToAdult();
			}
			else 
			{
				character.switchToChild();
			}
		}
		
		if(inputType.use())
		{
			//Use something... maybe.
		}
	}
}

