﻿using UnityEngine;
using System.Collections;

public class MonsterScript : MonoBehaviour, IObserver {

	private Vector3 direction;
	private float speed = 10;
	private float previousTime;

	// Use this for initialization
	void Start () {
		direction = new Vector3(1,0,0);
		previousTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time - previousTime > 2)
		{
			direction.x = -direction.x;
			previousTime = Time.time;
		}

		transform.position += direction*speed*Time.deltaTime;
	}

	public void notify()
	{
		//Switch state.
	}
}
