﻿using UnityEngine;
using System.Collections;

public class MainGameScript : MonoBehaviour {

	public int healthMeter = 100;
	public int insanityMeter = 100;
	public bool isSane = true;
	public bool isSlightlyInsane = false;
	public bool isNearCompletelyInsane = false;
	public bool isCompletelyInsane = false;
	public bool playerIsChild;

	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (healthMeter == 0) 
		{  Application.LoadLevel("GameOverScreen");
			Debug.Log ("Game Over");
		}

		if (insanityMeter <= 67) 
		{
			isSane = false;
			isSlightlyInsane = true;
		}

		if (insanityMeter <= 34) 
		{
			isNearCompletelyInsane = true;
		}
		if (insanityMeter == 0) 
		{
			isCompletelyInsane = true;
		}
	}
}
