﻿using UnityEngine;
using System.Collections;

public class KeyboardGamePlayerInput : IPlayerInput 
{

	public bool left()
	{
		return Input.GetKey(KeyCode.LeftArrow);
	}

	public bool right()
	{
		return Input.GetKey(KeyCode.RightArrow);
	}

	public bool up()
	{
		return Input.GetKeyDown(KeyCode.UpArrow);
	}

	public bool down()
	{
		return Input.GetKey(KeyCode.DownArrow);
	}

	public bool pause()
	{
		return Input.GetKeyDown(KeyCode.Escape);
	}

	public bool resume()
	{
		return Input.GetKeyDown(KeyCode.Escape);
	}

	public bool switchPersona()
	{
		return Input.GetKeyDown(KeyCode.RightShift);
	}

	public bool use()
	{
		return Input.GetKeyDown(KeyCode.Return);
	}

}
