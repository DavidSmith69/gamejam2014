﻿using UnityEngine;
using System.Collections;

public class MenuInterface : MonoBehaviour {
	// Creates three buttons, prints message when clicked.
	// Catches errors if no textures are assigned.
	public GUIStyle playTexture;
	public GUIStyle howToTexture;
	public GUIStyle quitTexture;

	void OnGUI() {
			/*	if (!playTexture) {
					//	Debug.LogError ("Please assign a texture for Play Button on the inspector");
					//	return;
				//}
		if (!howToTexture) {
			Debug.LogError ("Please assign a texture for How To Button on the inspector");
			return;
		}
		if (!quitTexture) {
			Debug.LogError ("Please assign a texture for Quit Button on the inspector");
			return;
		}*/
		//All Play Button Attributes
				if (GUI.Button (new Rect ((Screen.width/2) + 425,(Screen.height/2) - 260, 150, 50),"", playTexture)) {
			Application.LoadLevel ("FirstLevel");
						Debug.Log ("Clicked the Start Button");
				}
		//All How to Play Button Attribtues
				if (GUI.Button (new Rect ((Screen.width/2) + 425, (Screen.height/2) - 170, 150, 50),"", howToTexture)) {
			Debug.Log ("Clicked the How to Play Button");
			Application.LoadLevel("HowToLevel");
				}
		//All Quit Button Attributes
				if (GUI.Button (new Rect ((Screen.width/2) + 425, (Screen.height/2) - 90, 150, 50),"", quitTexture)) {
			Debug.Log ("Clicked the Quit Button");
			Application.Quit();
				}
		}

		// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	

}
