using System;

public interface IObserver
{
	void notify();
}

