﻿using UnityEngine;
using System.Collections;

public interface IPlayerInput 
{
	bool left();
	bool right();
	bool up();
	bool down();
	bool pause();
	bool resume();
	bool switchPersona();
	bool use();
}
