﻿using UnityEngine;
using System.Collections;

public class BedScript : MonoBehaviour, IObserver {

	public GameObject mainCharacterObj;
	public GameObject tentacleMonsterPrefab;
	private bool currentStateIsChild;

	private CharacterScript characterScript;
	// Use this for initialization
	void Start () {
		characterScript = mainCharacterObj.GetComponent("CharacterScript") as CharacterScript;
		currentStateIsChild = characterScript.isChild;
	}
	
	// Update is called once per frame
	void Update () {
		if(currentStateIsChild)
			beScary();
	}

	private void switchState()
	{
		currentStateIsChild = !currentStateIsChild;

		if(currentStateIsChild)
			renderer.material.mainTexture = renderer.materials[0].mainTexture;
		else
			renderer.material.mainTexture = renderer.materials[1].mainTexture;
	}

	private void beScary()
	{
		Instantiate(tentacleMonsterPrefab, transform.position, Quaternion.identity);
	}

	public void notify()
	{
		switchState();
	}
}
